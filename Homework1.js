// Теоретичні питання
// Як можна оголосити змінну у Javascript?
// const; let; (var);

// У чому різниця між функцією prompt та функцією confirm?
// function confirm - отображает модальное окно с вопросом и двумя кнопками: ОК, Отмена. Результат true, false
// В отличие от confirm prompt показывает сообщение и запрашивает ввод текста и возвращает введенный текст или null.

// Що таке неявне перетворення типів? Наведіть один приклад.
// Неявное преобразование происходит, когда мы заставляем JavaScript работать со значениями разных типов.
// 5 + "5" === "55";
// Завдання
// Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.


// let name = 'Ihor';
// let admin = name;
// console.log(admin);
let name = 'Ihor';
let admin;
admin = name;
console.log(admin);

// Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.
let days;
for (let days = 1;  days < 10; days++) {
    // console.log(days);
    let sec = Math.floor(days * 24 * 60 * 60);
    console.log(`Days: ${days}, seconds: ${sec}.`);
}

// Запитайте у користувача якесь значення і виведіть його в консоль.

let userName = prompt("What is your name?");
let userAge = prompt("How old are you?");
console.log(`Hello ${userName}, you have only ${userAge} years`);